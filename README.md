# OpenDesign Platform

OpenDesign is platform for connecting designers with Open Source Projects. We aim to create a solid framework so that other contributors can join this project and take it to the next level.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please read CONTRIBUTING.md and CODE_OF_CONDUCT for details on our code of conduct, and the process for submitting pull requests to us.

## License

[MIT](https://choosealicense.com/licenses/mit/)
