First off, thank you for considering contributing to OpenDesign.

Following these guidelines helps to communicate that you respect the time of the designers and developers managing and developing this open source project. In return, they should reciprocate that respect in addressing your issue, assessing changes, and helping you finalize your pull requests.

OpenDesign is an open source project and we love to receive contributions from our community — you! There are many ways to contribute, from continuing the design thinking process, designing wireframes, writing blog posts, improving the documentation, or submitting feature requests which can be incorporated into OpenDesign itself.

OpenDesign is a new project. Our purpose is to help more designers contribute to Open Source projects. We started the design thinking process, and we need help taking it to the next level.

To make a contribution you can open an issue and describe your observation or your concern. 

Thank you for taking an interest in OpenDesign!
