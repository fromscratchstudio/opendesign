# Product Vision, Opportunities, and Hypotheses
This board contains all information regarding the product vision, strategic objectives, the identified product opportunities (user needs, problems, generic opportunities), and the proposed product solutions (defined as hypotheses and candidate functionalities). The file is available for commenting.
https://miro.com/app/board/uXjVNXgYMxU=/?share_link_id=999409576600
